class AddSessionUserToRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :records, :session_user_id, :string
  end
end
