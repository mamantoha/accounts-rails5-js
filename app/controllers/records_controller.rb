class RecordsController < ApplicationController
  before_action :set_record, only: [:update, :destroy]

  # GET /records
  # GET /records.json
  def index
    load_records
    @record = Record.new(date: Date.today)
  end

  # POST /records
  # POST /records.json
  def create
    @record = Record.belonging_to(session_user).new(record_params)

    respond_to do |format|
      if @result = @record.save
        load_records

        format.html { redirect_to @record, notice: 'Record was successfully created.' }
        format.json { render :show, status: :created, location: @record }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.js { render json: @record.errors.full_messages, status: :unprocessable_entity }
        # format.js   {}
      end
    end
  end

  # PATCH/PUT /records/1
  # PATCH/PUT /records/1.json
  def update
    respond_to do |format|
      if @record.update(record_params)
        load_records

        format.html { redirect_to @record, notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @record }
        format.js {}
      else
        format.html { render :edit }
        format.json { render json: @record.errors, status: :unprocessable_entity }
        format.js   { render json: @record.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /records/1
  # DELETE /records/1.json
  def destroy
    @record.destroy
    load_records

    respond_to do |format|
      format.html { redirect_to records_url, notice: 'Record was successfully destroyed.' }
      format.json { head :no_content }
      format.js {}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_record
      @record = load_records.find(params[:id])
    end

    def load_records
      @records = Record.belonging_to(session_user)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def record_params
      params.require(:record).permit(:title, :date, :amount)
    end
end
