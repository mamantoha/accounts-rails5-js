class Record < ApplicationRecord
  validates :title, :amount, :date, presence: true
  validates :amount, numericality: true

  scope :debit, -> { where("amount < ?", 0).sum(:amount) }
  scope :credit, -> { where("amount >= ?", 0).sum(:amount) }
  scope :balance, -> { sum(:amount) }

  scope :belonging_to, -> (session_user_id) { where(session_user_id: session_user_id) }
end
