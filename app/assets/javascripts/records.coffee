# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
class App.Record
  @allRecords = []

  constructor: (@record_partial) ->
    @id = @record_partial.attr('id')
    Record.allRecords.push {id: @id}
    @show_record = @record_partial.find('.js-show-record')
    @edit_record = @record_partial.find('.js-edit-record')

  renderShow: ->
    if $("##{@id}").length > 0
      $("##{@id}").replaceWith(@record_partial)
    else
      $('#records_table').append(@record_partial)
    @bindEvents()

  bindEvents: ->
    @show_record.on 'click', 'a#edit-record', @editRecord
    @edit_record.on 'click', 'input#update-record', @updateRecord
    @edit_record.on 'click', 'a#cancel-edit-record', @cancelEditRecord
    @show_record.on 'click', 'a#destroy-record-modal', @destroyRecordConfirm

  editRecord: (event) ->
    event.preventDefault()
    show_record = this.closest('tr')
    $(show_record).css({ display: 'none' })
    edit_record = $(show_record).next('tr')
    $(edit_record).show 1

  cancelEditRecord: (event) ->
    event.preventDefault()
    edit_record = this.closest('tr')
    $(edit_record).css({ display: 'none' })
    # restore default values of text fields
    $(edit_record).find("input[name='date']").val($(edit_record).find("input[name='date']").attr('value'))
    $(edit_record).find("input[name='title']").val($(edit_record).find("input[name='title']").attr('value'))
    $(edit_record).find("input[name='amount']").val($(edit_record).find("input[name='amount']").attr('value'))

    show_record = $(edit_record).prev('tr')
    $(show_record).show 1
    error = new App.ErrorMessage('', '')
    error.removeErrors()

  updateRecord: (event) ->
    event.preventDefault()
    url = this.closest('form').action
    edit_record = this.closest('tr')
    title = $(edit_record).find("input[name='title']").val()
    date = $(edit_record).find("input[name='date']").val()
    amount = $(edit_record).find("input[name='amount']").val()
    data = { title: title, date: date, amount: amount }
    $.ajax
      method: 'PUT'
      url: url
      dataType: 'script'
      data:
        record: data
      success: (data, status, xhr) =>
        # {readyState: 4, responseText: "console.log('update clicked')↵var record_partial =…-200.0")↵$('#counts #js-balance').html("-190.0")↵", status: 200, statusText: "OK"}
        error = new App.ErrorMessage(xhr, status)
        error.removeErrors()
      error: (xhr, status, error) =>
        # xhr => {readyState: 4, responseText: "["Date can't be blank"]", status: 422, statusText: "Unprocessable Entity"}
        error = new App.ErrorMessage(xhr, status)
        error.showErrors()

  destroyRecordConfirm: (event) ->
    event.preventDefault()
    event.stopPropagation()
    data = {
      id: $(this).data('id'),
      'title': $(this).data('title'),
      'url': $(this).data('url')
    }
    delete_modal = new App.RecordDestroyConfirmation(data)
    delete_modal.showModal()

$(document).ready ->
  $('.js-record').each (record) ->
    record = new App.Record($(this))
    record.renderShow()
