class App.ErrorMessage
  constructor: (@xhr, @status) ->
    #

  showErrors: () ->
    @removeErrors()
    switch @xhr.status
      when 422 then @unprocessable_entity_error()
      when 404 then @not_found_error()
      else # do nothing

  removeErrors: () ->
    $('.alert.alert-danger').remove()

  unprocessable_entity_error: () ->
    messages = JSON.parse(@xhr.responseText)

    msgElem = document.createElement('div')
    msgElem.className = 'alert alert-danger alert-dismissible fade in'
    msgElem.innerHTML += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>'

    msgElem.innerHTML += "<strong>#{messages.length} error(s) prohibited this record from being saved:</strong>"
    msgElem.innerHTML += "<ul>"
    for key, value of messages
      msgElem.innerHTML += "<li>#{value}</li>"
    msgElem.innerHTML += "</ul>"
    $('#notice .container').prepend(msgElem);

  not_found_error: () ->
    msgElem = document.createElement('div')
    msgElem.className = 'alert alert-danger alert-dismissible fade in'
    msgElem.innerHTML += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>'
    msgElem.innerHTML += "<strong>Record not found</strong>"
    $('#notice .container').prepend(msgElem);
