class App.RecordDestroyConfirmation
  constructor: (@data = {}) ->
    @modal = $('#delete-confirm')

  render: ->
    @bindEvents()

  bindEvents: () ->
    @modal.on 'shown.bs.modal', @renderModal
    @modal.on 'click', 'a#destroy-record', @destroyRecord

  showModal: ->
    @modal.data(@data).modal('show')

  renderModal: (e) ->
    modalDiv = e.delegateTarget
    id = $(this).data('id')
    url = $(this).data('url')
    title = $(this).data('title')
    $('.title', this).text(title)
    $submit = $(this).find('.btn-danger')
    $submit.attr 'href', url

  destroyRecord: (event) =>
    event.preventDefault()
    event.stopPropagation()
    url = @modal.find('#destroy-record')[0].href
    $.ajax
      method: 'DELETE'
      url: url
      dataType: 'script'
      success: (data, status, xhr) =>
        # do something
      error: (xhr, status, err) =>
        error = new App.ErrorMessage(xhr, status)
        error.showErrors()
      complete: (xht, status) =>
        @modal.modal('hide')

$(document).ready ->
  record_destroy_confirmation = new App.RecordDestroyConfirmation
  record_destroy_confirmation.render()
