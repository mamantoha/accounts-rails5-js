class App.RecordForm
  constructor: (@form_partial) ->
    @submit_btn = $(@form_partial).find('input[type="submit"]')
    @cancel_btn = $(@form_partial).find('button[type="reset"]')
    # prevent the :disable_with helper from enabling the button again after submitting form
    @submit_btn.removeAttr('data-disable-with')
    @submit_btn.prop('disabled', true)
    @inputs = $(@form_partial).find('input')
    @inputs.keyup(@checkSubmit).each =>
      @checkSubmit
    @inputs.change(@checkSubmit).each =>
      @checkSubmit
    @bindEvent()

  bindEvent: ->
    @form_partial.on 'click', '#create-record-submit', @createRecord
    @cancel_btn.on 'click', @cancelCreateRecord

  createRecord: (event) =>
    event.preventDefault()
    new_record_form = @form_partial
    url = new_record_form.attr('action')
    date = $(new_record_form).find("input[id='record_date']").val()
    title = $(new_record_form).find("input[id='record_title']").val()
    amount = $(new_record_form).find("input[id='record_amount']").val()
    data = { title: title, date: date, amount: amount }
    $.ajax
      method: 'POST'
      url: url
      dataType: 'script'
      data:
        record: data
      success: (data, status, xhr) =>
        @resetForm()
        error = new App.ErrorMessage(xhr, status)
        error.removeErrors()
      error: (xhr, status, error) =>
        error = new App.ErrorMessage(xhr, status)
        error.showErrors()

  resetForm: ->
    @form_partial[0].reset()
    @submit_btn.prop('disabled', true)

  cancelCreateRecord: (event) ->
    error = new App.ErrorMessage('', '')
    error.removeErrors()

  validateForm: ->
    is_valid = true
    @form_partial.find('input').each ->
      if $(this).val() == ''
        is_valid = false
    is_valid

  checkSubmit: =>
    if @validateForm()
      @submit_btn.prop('disabled', false)
    else
      @submit_btn.prop('disabled', true)

$(document).ready ->
  record_form = new App.RecordForm($('#new_record'))
